import { Component } from '@angular/core';

@Component({
    selector: 'app-create-truth-game',
    templateUrl: './create-truth-game.component.html',
    styleUrls: ['./create-truth-game.component.scss'],
})
export class CreateTruthGameComponent {
    initialLink: string = '../../../assets/images/feu.png';
    activeLink: string = '../../../assets/images/feu2.png';
    active: boolean[] = [false, false, false];
    active1: boolean = false;
    active2: boolean = false;
    active3: boolean = false;

    handleImg(img: number) {
        switch (img) {
            case 0:
                if (!this.active[0]) this.activeImg();
                else {
                    this.unactiveImg();
                }
                break;
            case 1:
                if (!this.active[1]) this.activeImg();
                else {
                    this.unactiveImg();
                }
                break;
            case 2:
                if (!this.active[2]) this.activeImg();
                else {
                    this.unactiveImg();
                }
                break;
        }
    }

    unactiveImg() {
        for (let i = 2; i >= 0; i--) {
            if (this.active[i]) {
                this.active[i] = false;
                return;
            }
        }
    }

    activeImg() {
        for (let i = 0; i <= 2; i++) {
            if (!this.active[i]) {
                this.active[i] = true;
                return;
            }
        }
    }
}
