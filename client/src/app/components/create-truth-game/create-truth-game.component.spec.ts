import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateTruthGameComponent } from './create-truth-game.component';

describe('CreateTruthGameComponent', () => {
  let component: CreateTruthGameComponent;
  let fixture: ComponentFixture<CreateTruthGameComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CreateTruthGameComponent]
    });
    fixture = TestBed.createComponent(CreateTruthGameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
