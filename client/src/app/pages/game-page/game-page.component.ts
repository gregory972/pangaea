import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-game-page',
    templateUrl: './game-page.component.html',
    styleUrls: ['./game-page.component.scss'],
})
export class GamePageComponent {
    loupgarou: boolean = false;
    tictac: boolean = false;
    verite: boolean = false;

    constructor(private router: Router) {}

    handleButtonClick(game: string) {
        switch (game) {
            case 'Loup-Garou':
                this.tictac = false;
                this.verite = false;
                this.loupgarou = !this.loupgarou;
                break;
            case 'tictac':
                this.loupgarou = false;
                this.verite = false;
                this.tictac = !this.tictac;
                break;
            case 'verite':
                this.loupgarou = false;
                this.tictac = false;
                this.verite = !this.verite;
                this.router.navigate(['/truth']);
                break;
            default:
                alert('erreur evenement');
        }
    }
}
