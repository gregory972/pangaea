import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TruthPageComponent } from './truth-page.component';

describe('TruthPageComponent', () => {
  let component: TruthPageComponent;
  let fixture: ComponentFixture<TruthPageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TruthPageComponent]
    });
    fixture = TestBed.createComponent(TruthPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
