import { Component } from '@angular/core';

@Component({
  selector: 'app-truth-page',
  templateUrl: './truth-page.component.html',
  styleUrls: ['./truth-page.component.scss']
})
export class TruthPageComponent {

    createGame: boolean = false;

    handleCreateGame() {
      this.createGame = true;
    }
}
